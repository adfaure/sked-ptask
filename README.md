To generate the nix files

```
nix-shell --command "carnix generate-nix --src ./."
```

To build the packages:

```
nix-build
```
