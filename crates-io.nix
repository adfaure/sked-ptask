{ lib, buildRustCrate, buildRustCrateHelpers }:
with buildRustCrateHelpers;
let inherit (lib.lists) fold;
    inherit (lib.attrsets) recursiveUpdate;
in
rec {

# aho-corasick-0.6.10

  crates.aho_corasick."0.6.10" = deps: { features?(features_.aho_corasick."0.6.10" deps {}) }: buildRustCrate {
    crateName = "aho-corasick";
    version = "0.6.10";
    description = "Fast multiple substring searching with finite state machines.";
    authors = [ "Andrew Gallant <jamslam@gmail.com>" ];
    sha256 = "0bhasxfpmfmz1460chwsx59vdld05axvmk1nbp3sd48xav3d108p";
    libName = "aho_corasick";
    crateBin =
      [{  name = "aho-corasick-dot";  path = "src/main.rs"; }];
    dependencies = mapFeatures features ([
      (crates."memchr"."${deps."aho_corasick"."0.6.10"."memchr"}" deps)
    ]);
  };
  features_.aho_corasick."0.6.10" = deps: f: updateFeatures f (rec {
    aho_corasick."0.6.10".default = (f.aho_corasick."0.6.10".default or true);
    memchr."${deps.aho_corasick."0.6.10".memchr}".default = true;
  }) [
    (features_.memchr."${deps."aho_corasick"."0.6.10"."memchr"}" deps)
  ];


# end
# bitflags-1.1.0

  crates.bitflags."1.1.0" = deps: { features?(features_.bitflags."1.1.0" deps {}) }: buildRustCrate {
    crateName = "bitflags";
    version = "1.1.0";
    description = "A macro to generate structures which behave like bitflags.\n";
    authors = [ "The Rust Project Developers" ];
    sha256 = "1iwa4jrqcf4lnbwl562a3lx3r0jkh1j88b219bsqvbm4sni67dyv";
    build = "build.rs";
    features = mkFeatures (features."bitflags"."1.1.0" or {});
  };
  features_.bitflags."1.1.0" = deps: f: updateFeatures f (rec {
    bitflags."1.1.0".default = (f.bitflags."1.1.0".default or true);
  }) [];


# end
# cfg-if-0.1.9

  crates.cfg_if."0.1.9" = deps: { features?(features_.cfg_if."0.1.9" deps {}) }: buildRustCrate {
    crateName = "cfg-if";
    version = "0.1.9";
    description = "A macro to ergonomically define an item depending on a large number of #[cfg]\nparameters. Structured like an if-else chain, the first matching branch is the\nitem that gets emitted.\n";
    authors = [ "Alex Crichton <alex@alexcrichton.com>" ];
    sha256 = "13g9p2mc5b2b5wn716fwvilzib376ycpkgk868yxfp16jzix57p7";
  };
  features_.cfg_if."0.1.9" = deps: f: updateFeatures f (rec {
    cfg_if."0.1.9".default = (f.cfg_if."0.1.9".default or true);
  }) [];


# end
# docopt-0.8.3

  crates.docopt."0.8.3" = deps: { features?(features_.docopt."0.8.3" deps {}) }: buildRustCrate {
    crateName = "docopt";
    version = "0.8.3";
    description = "Command line argument parsing.";
    authors = [ "Andrew Gallant <jamslam@gmail.com>" ];
    sha256 = "1f8zzkgsakpdb5zqck13jkxj5f2sv397hjlbqiyfa57cq7sd0jvd";
    crateBin =
      [{  name = "docopt-wordlist";  path = "src/wordlist.rs"; }];
    dependencies = mapFeatures features ([
      (crates."lazy_static"."${deps."docopt"."0.8.3"."lazy_static"}" deps)
      (crates."regex"."${deps."docopt"."0.8.3"."regex"}" deps)
      (crates."serde"."${deps."docopt"."0.8.3"."serde"}" deps)
      (crates."serde_derive"."${deps."docopt"."0.8.3"."serde_derive"}" deps)
      (crates."strsim"."${deps."docopt"."0.8.3"."strsim"}" deps)
    ]);
  };
  features_.docopt."0.8.3" = deps: f: updateFeatures f (rec {
    docopt."0.8.3".default = (f.docopt."0.8.3".default or true);
    lazy_static."${deps.docopt."0.8.3".lazy_static}".default = true;
    regex."${deps.docopt."0.8.3".regex}".default = true;
    serde."${deps.docopt."0.8.3".serde}".default = true;
    serde_derive."${deps.docopt."0.8.3".serde_derive}".default = true;
    strsim."${deps.docopt."0.8.3".strsim}".default = true;
  }) [
    (features_.lazy_static."${deps."docopt"."0.8.3"."lazy_static"}" deps)
    (features_.regex."${deps."docopt"."0.8.3"."regex"}" deps)
    (features_.serde."${deps."docopt"."0.8.3"."serde"}" deps)
    (features_.serde_derive."${deps."docopt"."0.8.3"."serde_derive"}" deps)
    (features_.strsim."${deps."docopt"."0.8.3"."strsim"}" deps)
  ];


# end
# env_logger-0.4.3

  crates.env_logger."0.4.3" = deps: { features?(features_.env_logger."0.4.3" deps {}) }: buildRustCrate {
    crateName = "env_logger";
    version = "0.4.3";
    description = "A logging implementation for `log` which is configured via an environment\nvariable.\n";
    authors = [ "The Rust Project Developers" ];
    sha256 = "0nrx04p4xa86d5kc7aq4fwvipbqji9cmgy449h47nc9f1chafhgg";
    dependencies = mapFeatures features ([
      (crates."log"."${deps."env_logger"."0.4.3"."log"}" deps)
    ]
      ++ (if features.env_logger."0.4.3".regex or false then [ (crates.regex."${deps."env_logger"."0.4.3".regex}" deps) ] else []));
    features = mkFeatures (features."env_logger"."0.4.3" or {});
  };
  features_.env_logger."0.4.3" = deps: f: updateFeatures f (rec {
    env_logger = fold recursiveUpdate {} [
      { "0.4.3"."regex" =
        (f.env_logger."0.4.3"."regex" or false) ||
        (f.env_logger."0.4.3".default or false) ||
        (env_logger."0.4.3"."default" or false); }
      { "0.4.3".default = (f.env_logger."0.4.3".default or true); }
    ];
    log."${deps.env_logger."0.4.3".log}".default = true;
    regex."${deps.env_logger."0.4.3".regex}".default = true;
  }) [
    (features_.log."${deps."env_logger"."0.4.3"."log"}" deps)
    (features_.regex."${deps."env_logger"."0.4.3"."regex"}" deps)
  ];


# end
# error-chain-0.10.0

  crates.error_chain."0.10.0" = deps: { features?(features_.error_chain."0.10.0" deps {}) }: buildRustCrate {
    crateName = "error-chain";
    version = "0.10.0";
    description = "Yet another error boilerplate library.";
    authors = [ "Brian Anderson <banderson@mozilla.com>" "Paul Colomiets <paul@colomiets.name>" "Colin Kiegel <kiegel@gmx.de>" "Yamakaky <yamakaky@yamaworld.fr>" ];
    sha256 = "1xxbzd8cjlpzsb9fsih7mdnndhzrvykj0w77yg90qc85az1xwy5z";
    dependencies = mapFeatures features ([
]);
    features = mkFeatures (features."error_chain"."0.10.0" or {});
  };
  features_.error_chain."0.10.0" = deps: f: updateFeatures f (rec {
    error_chain = fold recursiveUpdate {} [
      { "0.10.0"."backtrace" =
        (f.error_chain."0.10.0"."backtrace" or false) ||
        (f.error_chain."0.10.0".default or false) ||
        (error_chain."0.10.0"."default" or false); }
      { "0.10.0"."example_generated" =
        (f.error_chain."0.10.0"."example_generated" or false) ||
        (f.error_chain."0.10.0".default or false) ||
        (error_chain."0.10.0"."default" or false); }
      { "0.10.0".default = (f.error_chain."0.10.0".default or true); }
    ];
  }) [];


# end
# itoa-0.4.4

  crates.itoa."0.4.4" = deps: { features?(features_.itoa."0.4.4" deps {}) }: buildRustCrate {
    crateName = "itoa";
    version = "0.4.4";
    description = "Fast functions for printing integer primitives to an io::Write";
    authors = [ "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "1fqc34xzzl2spfdawxd9awhzl0fwf1y6y4i94l8bq8rfrzd90awl";
    features = mkFeatures (features."itoa"."0.4.4" or {});
  };
  features_.itoa."0.4.4" = deps: f: updateFeatures f (rec {
    itoa = fold recursiveUpdate {} [
      { "0.4.4"."std" =
        (f.itoa."0.4.4"."std" or false) ||
        (f.itoa."0.4.4".default or false) ||
        (itoa."0.4.4"."default" or false); }
      { "0.4.4".default = (f.itoa."0.4.4".default or true); }
    ];
  }) [];


# end
# lazy_static-1.3.0

  crates.lazy_static."1.3.0" = deps: { features?(features_.lazy_static."1.3.0" deps {}) }: buildRustCrate {
    crateName = "lazy_static";
    version = "1.3.0";
    description = "A macro for declaring lazily evaluated statics in Rust.";
    authors = [ "Marvin Löbel <loebel.marvin@gmail.com>" ];
    sha256 = "1vv47va18ydk7dx5paz88g3jy1d3lwbx6qpxkbj8gyfv770i4b1y";
    dependencies = mapFeatures features ([
]);
    features = mkFeatures (features."lazy_static"."1.3.0" or {});
  };
  features_.lazy_static."1.3.0" = deps: f: updateFeatures f (rec {
    lazy_static = fold recursiveUpdate {} [
      { "1.3.0"."spin" =
        (f.lazy_static."1.3.0"."spin" or false) ||
        (f.lazy_static."1.3.0".spin_no_std or false) ||
        (lazy_static."1.3.0"."spin_no_std" or false); }
      { "1.3.0".default = (f.lazy_static."1.3.0".default or true); }
    ];
  }) [];


# end
# libc-0.2.60

  crates.libc."0.2.60" = deps: { features?(features_.libc."0.2.60" deps {}) }: buildRustCrate {
    crateName = "libc";
    version = "0.2.60";
    description = "Raw FFI bindings to platform libraries like libc.\n";
    authors = [ "The Rust Project Developers" ];
    sha256 = "1zklw10b6lwz6ldamxvdxr8gsxbqhphxhn8n5n5dndl7avafx49b";
    build = "build.rs";
    dependencies = mapFeatures features ([
]);
    features = mkFeatures (features."libc"."0.2.60" or {});
  };
  features_.libc."0.2.60" = deps: f: updateFeatures f (rec {
    libc = fold recursiveUpdate {} [
      { "0.2.60"."align" =
        (f.libc."0.2.60"."align" or false) ||
        (f.libc."0.2.60".rustc-dep-of-std or false) ||
        (libc."0.2.60"."rustc-dep-of-std" or false); }
      { "0.2.60"."rustc-std-workspace-core" =
        (f.libc."0.2.60"."rustc-std-workspace-core" or false) ||
        (f.libc."0.2.60".rustc-dep-of-std or false) ||
        (libc."0.2.60"."rustc-dep-of-std" or false); }
      { "0.2.60"."std" =
        (f.libc."0.2.60"."std" or false) ||
        (f.libc."0.2.60".default or false) ||
        (libc."0.2.60"."default" or false) ||
        (f.libc."0.2.60".use_std or false) ||
        (libc."0.2.60"."use_std" or false); }
      { "0.2.60".default = (f.libc."0.2.60".default or true); }
    ];
  }) [];


# end
# log-0.3.9

  crates.log."0.3.9" = deps: { features?(features_.log."0.3.9" deps {}) }: buildRustCrate {
    crateName = "log";
    version = "0.3.9";
    description = "A lightweight logging facade for Rust\n";
    authors = [ "The Rust Project Developers" ];
    sha256 = "19i9pwp7lhaqgzangcpw00kc3zsgcqcx84crv07xgz3v7d3kvfa2";
    dependencies = mapFeatures features ([
      (crates."log"."${deps."log"."0.3.9"."log"}" deps)
    ]);
    features = mkFeatures (features."log"."0.3.9" or {});
  };
  features_.log."0.3.9" = deps: f: updateFeatures f (rec {
    log = fold recursiveUpdate {} [
      { "${deps.log."0.3.9".log}"."max_level_debug" =
        (f.log."${deps.log."0.3.9".log}"."max_level_debug" or false) ||
        (log."0.3.9"."max_level_debug" or false) ||
        (f."log"."0.3.9"."max_level_debug" or false); }
      { "${deps.log."0.3.9".log}"."max_level_error" =
        (f.log."${deps.log."0.3.9".log}"."max_level_error" or false) ||
        (log."0.3.9"."max_level_error" or false) ||
        (f."log"."0.3.9"."max_level_error" or false); }
      { "${deps.log."0.3.9".log}"."max_level_info" =
        (f.log."${deps.log."0.3.9".log}"."max_level_info" or false) ||
        (log."0.3.9"."max_level_info" or false) ||
        (f."log"."0.3.9"."max_level_info" or false); }
      { "${deps.log."0.3.9".log}"."max_level_off" =
        (f.log."${deps.log."0.3.9".log}"."max_level_off" or false) ||
        (log."0.3.9"."max_level_off" or false) ||
        (f."log"."0.3.9"."max_level_off" or false); }
      { "${deps.log."0.3.9".log}"."max_level_trace" =
        (f.log."${deps.log."0.3.9".log}"."max_level_trace" or false) ||
        (log."0.3.9"."max_level_trace" or false) ||
        (f."log"."0.3.9"."max_level_trace" or false); }
      { "${deps.log."0.3.9".log}"."max_level_warn" =
        (f.log."${deps.log."0.3.9".log}"."max_level_warn" or false) ||
        (log."0.3.9"."max_level_warn" or false) ||
        (f."log"."0.3.9"."max_level_warn" or false); }
      { "${deps.log."0.3.9".log}"."release_max_level_debug" =
        (f.log."${deps.log."0.3.9".log}"."release_max_level_debug" or false) ||
        (log."0.3.9"."release_max_level_debug" or false) ||
        (f."log"."0.3.9"."release_max_level_debug" or false); }
      { "${deps.log."0.3.9".log}"."release_max_level_error" =
        (f.log."${deps.log."0.3.9".log}"."release_max_level_error" or false) ||
        (log."0.3.9"."release_max_level_error" or false) ||
        (f."log"."0.3.9"."release_max_level_error" or false); }
      { "${deps.log."0.3.9".log}"."release_max_level_info" =
        (f.log."${deps.log."0.3.9".log}"."release_max_level_info" or false) ||
        (log."0.3.9"."release_max_level_info" or false) ||
        (f."log"."0.3.9"."release_max_level_info" or false); }
      { "${deps.log."0.3.9".log}"."release_max_level_off" =
        (f.log."${deps.log."0.3.9".log}"."release_max_level_off" or false) ||
        (log."0.3.9"."release_max_level_off" or false) ||
        (f."log"."0.3.9"."release_max_level_off" or false); }
      { "${deps.log."0.3.9".log}"."release_max_level_trace" =
        (f.log."${deps.log."0.3.9".log}"."release_max_level_trace" or false) ||
        (log."0.3.9"."release_max_level_trace" or false) ||
        (f."log"."0.3.9"."release_max_level_trace" or false); }
      { "${deps.log."0.3.9".log}"."release_max_level_warn" =
        (f.log."${deps.log."0.3.9".log}"."release_max_level_warn" or false) ||
        (log."0.3.9"."release_max_level_warn" or false) ||
        (f."log"."0.3.9"."release_max_level_warn" or false); }
      { "${deps.log."0.3.9".log}"."std" =
        (f.log."${deps.log."0.3.9".log}"."std" or false) ||
        (log."0.3.9"."use_std" or false) ||
        (f."log"."0.3.9"."use_std" or false); }
      { "${deps.log."0.3.9".log}".default = true; }
      { "0.3.9"."use_std" =
        (f.log."0.3.9"."use_std" or false) ||
        (f.log."0.3.9".default or false) ||
        (log."0.3.9"."default" or false); }
      { "0.3.9".default = (f.log."0.3.9".default or true); }
    ];
  }) [
    (features_.log."${deps."log"."0.3.9"."log"}" deps)
  ];


# end
# log-0.4.7

  crates.log."0.4.7" = deps: { features?(features_.log."0.4.7" deps {}) }: buildRustCrate {
    crateName = "log";
    version = "0.4.7";
    description = "A lightweight logging facade for Rust\n";
    authors = [ "The Rust Project Developers" ];
    sha256 = "0l5y0kd63l6mpw68r74asgk59rwqxmcjz8azjk9fax04r3gyzh05";
    build = "build.rs";
    dependencies = mapFeatures features ([
      (crates."cfg_if"."${deps."log"."0.4.7"."cfg_if"}" deps)
    ]);
    features = mkFeatures (features."log"."0.4.7" or {});
  };
  features_.log."0.4.7" = deps: f: updateFeatures f (rec {
    cfg_if."${deps.log."0.4.7".cfg_if}".default = true;
    log."0.4.7".default = (f.log."0.4.7".default or true);
  }) [
    (features_.cfg_if."${deps."log"."0.4.7"."cfg_if"}" deps)
  ];


# end
# memchr-2.2.1

  crates.memchr."2.2.1" = deps: { features?(features_.memchr."2.2.1" deps {}) }: buildRustCrate {
    crateName = "memchr";
    version = "2.2.1";
    description = "Safe interface to memchr.";
    authors = [ "Andrew Gallant <jamslam@gmail.com>" "bluss" ];
    sha256 = "1mj5z8lhz6jbapslpq8a39pwcsl1p0jmgp7wgcj7nv4pcqhya7a0";
    dependencies = mapFeatures features ([
]);
    features = mkFeatures (features."memchr"."2.2.1" or {});
  };
  features_.memchr."2.2.1" = deps: f: updateFeatures f (rec {
    memchr = fold recursiveUpdate {} [
      { "2.2.1"."use_std" =
        (f.memchr."2.2.1"."use_std" or false) ||
        (f.memchr."2.2.1".default or false) ||
        (memchr."2.2.1"."default" or false); }
      { "2.2.1".default = (f.memchr."2.2.1".default or true); }
    ];
  }) [];


# end
# metadeps-1.1.2

  crates.metadeps."1.1.2" = deps: { features?(features_.metadeps."1.1.2" deps {}) }: buildRustCrate {
    crateName = "metadeps";
    version = "1.1.2";
    description = "Run pkg-config from declarative dependencies in Cargo.toml";
    authors = [ "Josh Triplett <josh@joshtriplett.org>" ];
    sha256 = "00dpxjls9fq6fs5gr9v3hkqxmb1zwnhh8b56q3dnzghppjf3ivk3";
    dependencies = mapFeatures features ([
      (crates."error_chain"."${deps."metadeps"."1.1.2"."error_chain"}" deps)
      (crates."pkg_config"."${deps."metadeps"."1.1.2"."pkg_config"}" deps)
      (crates."toml"."${deps."metadeps"."1.1.2"."toml"}" deps)
    ]);
  };
  features_.metadeps."1.1.2" = deps: f: updateFeatures f (rec {
    error_chain."${deps.metadeps."1.1.2".error_chain}".default = (f.error_chain."${deps.metadeps."1.1.2".error_chain}".default or false);
    metadeps."1.1.2".default = (f.metadeps."1.1.2".default or true);
    pkg_config."${deps.metadeps."1.1.2".pkg_config}".default = true;
    toml."${deps.metadeps."1.1.2".toml}".default = (f.toml."${deps.metadeps."1.1.2".toml}".default or false);
  }) [
    (features_.error_chain."${deps."metadeps"."1.1.2"."error_chain"}" deps)
    (features_.pkg_config."${deps."metadeps"."1.1.2"."pkg_config"}" deps)
    (features_.toml."${deps."metadeps"."1.1.2"."toml"}" deps)
  ];


# end
# pkg-config-0.3.14

  crates.pkg_config."0.3.14" = deps: { features?(features_.pkg_config."0.3.14" deps {}) }: buildRustCrate {
    crateName = "pkg-config";
    version = "0.3.14";
    description = "A library to run the pkg-config system tool at build time in order to be used in\nCargo build scripts.\n";
    authors = [ "Alex Crichton <alex@alexcrichton.com>" ];
    sha256 = "0207fsarrm412j0dh87lfcas72n8mxar7q3mgflsbsrqnb140sv6";
  };
  features_.pkg_config."0.3.14" = deps: f: updateFeatures f (rec {
    pkg_config."0.3.14".default = (f.pkg_config."0.3.14".default or true);
  }) [];


# end
# proc-macro2-0.4.30

  crates.proc_macro2."0.4.30" = deps: { features?(features_.proc_macro2."0.4.30" deps {}) }: buildRustCrate {
    crateName = "proc-macro2";
    version = "0.4.30";
    description = "A stable implementation of the upcoming new `proc_macro` API. Comes with an\noption, off by default, to also reimplement itself in terms of the upstream\nunstable API.\n";
    authors = [ "Alex Crichton <alex@alexcrichton.com>" ];
    sha256 = "0iifv51wrm6r4r2gghw6rray3nv53zcap355bbz1nsmbhj5s09b9";
    build = "build.rs";
    dependencies = mapFeatures features ([
      (crates."unicode_xid"."${deps."proc_macro2"."0.4.30"."unicode_xid"}" deps)
    ]);
    features = mkFeatures (features."proc_macro2"."0.4.30" or {});
  };
  features_.proc_macro2."0.4.30" = deps: f: updateFeatures f (rec {
    proc_macro2 = fold recursiveUpdate {} [
      { "0.4.30"."proc-macro" =
        (f.proc_macro2."0.4.30"."proc-macro" or false) ||
        (f.proc_macro2."0.4.30".default or false) ||
        (proc_macro2."0.4.30"."default" or false); }
      { "0.4.30".default = (f.proc_macro2."0.4.30".default or true); }
    ];
    unicode_xid."${deps.proc_macro2."0.4.30".unicode_xid}".default = true;
  }) [
    (features_.unicode_xid."${deps."proc_macro2"."0.4.30"."unicode_xid"}" deps)
  ];


# end
# procset-0.1.2

  crates.procset."0.1.2" = deps: { features?(features_.procset."0.1.2" deps {}) }: buildRustCrate {
    crateName = "procset";
    version = "0.1.2";
    description = "  Toolkit to manage sets of closed intervals.\n";
    authors = [ "adfaure <adrien.faure2@gmail.com>" ];
    sha256 = "0s85ziy1acdxrln92q1c5kbr37wcv51xijzz9aa0kh4f7ib4y0sv";
    libPath = "src/libinterval_set/lib.rs";
    libName = "interval_set";
  };
  features_.procset."0.1.2" = deps: f: updateFeatures f (rec {
    procset."0.1.2".default = (f.procset."0.1.2".default or true);
  }) [];


# end
# quote-0.6.13

  crates.quote."0.6.13" = deps: { features?(features_.quote."0.6.13" deps {}) }: buildRustCrate {
    crateName = "quote";
    version = "0.6.13";
    description = "Quasi-quoting macro quote!(...)";
    authors = [ "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "1hrvsin40i4q8swrhlj9057g7nsp0lg02h8zbzmgz14av9mzv8g8";
    dependencies = mapFeatures features ([
      (crates."proc_macro2"."${deps."quote"."0.6.13"."proc_macro2"}" deps)
    ]);
    features = mkFeatures (features."quote"."0.6.13" or {});
  };
  features_.quote."0.6.13" = deps: f: updateFeatures f (rec {
    proc_macro2 = fold recursiveUpdate {} [
      { "${deps.quote."0.6.13".proc_macro2}"."proc-macro" =
        (f.proc_macro2."${deps.quote."0.6.13".proc_macro2}"."proc-macro" or false) ||
        (quote."0.6.13"."proc-macro" or false) ||
        (f."quote"."0.6.13"."proc-macro" or false); }
      { "${deps.quote."0.6.13".proc_macro2}".default = (f.proc_macro2."${deps.quote."0.6.13".proc_macro2}".default or false); }
    ];
    quote = fold recursiveUpdate {} [
      { "0.6.13"."proc-macro" =
        (f.quote."0.6.13"."proc-macro" or false) ||
        (f.quote."0.6.13".default or false) ||
        (quote."0.6.13"."default" or false); }
      { "0.6.13".default = (f.quote."0.6.13".default or true); }
    ];
  }) [
    (features_.proc_macro2."${deps."quote"."0.6.13"."proc_macro2"}" deps)
  ];


# end
# regex-0.2.11

  crates.regex."0.2.11" = deps: { features?(features_.regex."0.2.11" deps {}) }: buildRustCrate {
    crateName = "regex";
    version = "0.2.11";
    description = "An implementation of regular expressions for Rust. This implementation uses\nfinite automata and guarantees linear time matching on all inputs.\n";
    authors = [ "The Rust Project Developers" ];
    sha256 = "0r50cymxdqp0fv1dxd22mjr6y32q450nwacd279p9s7lh0cafijj";
    dependencies = mapFeatures features ([
      (crates."aho_corasick"."${deps."regex"."0.2.11"."aho_corasick"}" deps)
      (crates."memchr"."${deps."regex"."0.2.11"."memchr"}" deps)
      (crates."regex_syntax"."${deps."regex"."0.2.11"."regex_syntax"}" deps)
      (crates."thread_local"."${deps."regex"."0.2.11"."thread_local"}" deps)
      (crates."utf8_ranges"."${deps."regex"."0.2.11"."utf8_ranges"}" deps)
    ]);
    features = mkFeatures (features."regex"."0.2.11" or {});
  };
  features_.regex."0.2.11" = deps: f: updateFeatures f (rec {
    aho_corasick."${deps.regex."0.2.11".aho_corasick}".default = true;
    memchr."${deps.regex."0.2.11".memchr}".default = true;
    regex = fold recursiveUpdate {} [
      { "0.2.11"."pattern" =
        (f.regex."0.2.11"."pattern" or false) ||
        (f.regex."0.2.11".unstable or false) ||
        (regex."0.2.11"."unstable" or false); }
      { "0.2.11".default = (f.regex."0.2.11".default or true); }
    ];
    regex_syntax."${deps.regex."0.2.11".regex_syntax}".default = true;
    thread_local."${deps.regex."0.2.11".thread_local}".default = true;
    utf8_ranges."${deps.regex."0.2.11".utf8_ranges}".default = true;
  }) [
    (features_.aho_corasick."${deps."regex"."0.2.11"."aho_corasick"}" deps)
    (features_.memchr."${deps."regex"."0.2.11"."memchr"}" deps)
    (features_.regex_syntax."${deps."regex"."0.2.11"."regex_syntax"}" deps)
    (features_.thread_local."${deps."regex"."0.2.11"."thread_local"}" deps)
    (features_.utf8_ranges."${deps."regex"."0.2.11"."utf8_ranges"}" deps)
  ];


# end
# regex-syntax-0.5.6

  crates.regex_syntax."0.5.6" = deps: { features?(features_.regex_syntax."0.5.6" deps {}) }: buildRustCrate {
    crateName = "regex-syntax";
    version = "0.5.6";
    description = "A regular expression parser.";
    authors = [ "The Rust Project Developers" ];
    sha256 = "10vf3r34bgjnbrnqd5aszn35bjvm8insw498l1vjy8zx5yms3427";
    dependencies = mapFeatures features ([
      (crates."ucd_util"."${deps."regex_syntax"."0.5.6"."ucd_util"}" deps)
    ]);
  };
  features_.regex_syntax."0.5.6" = deps: f: updateFeatures f (rec {
    regex_syntax."0.5.6".default = (f.regex_syntax."0.5.6".default or true);
    ucd_util."${deps.regex_syntax."0.5.6".ucd_util}".default = true;
  }) [
    (features_.ucd_util."${deps."regex_syntax"."0.5.6"."ucd_util"}" deps)
  ];


# end
# ryu-1.0.0

  crates.ryu."1.0.0" = deps: { features?(features_.ryu."1.0.0" deps {}) }: buildRustCrate {
    crateName = "ryu";
    version = "1.0.0";
    description = "Fast floating point to string conversion";
    authors = [ "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "0hysqba7hi31xw1jka8jh7qb4m9fx5l6vik55wpc3rpsg46cwgbf";
    build = "build.rs";
    dependencies = mapFeatures features ([
]);
    features = mkFeatures (features."ryu"."1.0.0" or {});
  };
  features_.ryu."1.0.0" = deps: f: updateFeatures f (rec {
    ryu."1.0.0".default = (f.ryu."1.0.0".default or true);
  }) [];


# end
# serde-1.0.95

  crates.serde."1.0.95" = deps: { features?(features_.serde."1.0.95" deps {}) }: buildRustCrate {
    crateName = "serde";
    version = "1.0.95";
    description = "A generic serialization/deserialization framework";
    authors = [ "Erick Tryzelaar <erick.tryzelaar@gmail.com>" "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "1jcdxykgldrlnjwjyipp2y3n2v62msmkwviany9c6s8ldjd3kznl";
    build = "build.rs";
    dependencies = mapFeatures features ([
]);
    features = mkFeatures (features."serde"."1.0.95" or {});
  };
  features_.serde."1.0.95" = deps: f: updateFeatures f (rec {
    serde = fold recursiveUpdate {} [
      { "1.0.95"."serde_derive" =
        (f.serde."1.0.95"."serde_derive" or false) ||
        (f.serde."1.0.95".derive or false) ||
        (serde."1.0.95"."derive" or false); }
      { "1.0.95"."std" =
        (f.serde."1.0.95"."std" or false) ||
        (f.serde."1.0.95".default or false) ||
        (serde."1.0.95"."default" or false); }
      { "1.0.95".default = (f.serde."1.0.95".default or true); }
    ];
  }) [];


# end
# serde_derive-1.0.95

  crates.serde_derive."1.0.95" = deps: { features?(features_.serde_derive."1.0.95" deps {}) }: buildRustCrate {
    crateName = "serde_derive";
    version = "1.0.95";
    description = "Macros 1.1 implementation of #[derive(Serialize, Deserialize)]";
    authors = [ "Erick Tryzelaar <erick.tryzelaar@gmail.com>" "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "0sqk7b79v8s4zp7bvs8l00b1x2c67qsbb8rh0997pigblyych8kz";
    procMacro = true;
    dependencies = mapFeatures features ([
      (crates."proc_macro2"."${deps."serde_derive"."1.0.95"."proc_macro2"}" deps)
      (crates."quote"."${deps."serde_derive"."1.0.95"."quote"}" deps)
      (crates."syn"."${deps."serde_derive"."1.0.95"."syn"}" deps)
    ]);
    features = mkFeatures (features."serde_derive"."1.0.95" or {});
  };
  features_.serde_derive."1.0.95" = deps: f: updateFeatures f (rec {
    proc_macro2."${deps.serde_derive."1.0.95".proc_macro2}".default = true;
    quote."${deps.serde_derive."1.0.95".quote}".default = true;
    serde_derive."1.0.95".default = (f.serde_derive."1.0.95".default or true);
    syn = fold recursiveUpdate {} [
      { "${deps.serde_derive."1.0.95".syn}"."visit" = true; }
      { "${deps.serde_derive."1.0.95".syn}".default = true; }
    ];
  }) [
    (features_.proc_macro2."${deps."serde_derive"."1.0.95"."proc_macro2"}" deps)
    (features_.quote."${deps."serde_derive"."1.0.95"."quote"}" deps)
    (features_.syn."${deps."serde_derive"."1.0.95"."syn"}" deps)
  ];


# end
# serde_json-1.0.40

  crates.serde_json."1.0.40" = deps: { features?(features_.serde_json."1.0.40" deps {}) }: buildRustCrate {
    crateName = "serde_json";
    version = "1.0.40";
    description = "A JSON serialization file format";
    authors = [ "Erick Tryzelaar <erick.tryzelaar@gmail.com>" "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "1wf8lkisjvyg4ghp2fwm3ysymjy66l030l8d7p6033wiayfzpyh3";
    dependencies = mapFeatures features ([
      (crates."itoa"."${deps."serde_json"."1.0.40"."itoa"}" deps)
      (crates."ryu"."${deps."serde_json"."1.0.40"."ryu"}" deps)
      (crates."serde"."${deps."serde_json"."1.0.40"."serde"}" deps)
    ]);
    features = mkFeatures (features."serde_json"."1.0.40" or {});
  };
  features_.serde_json."1.0.40" = deps: f: updateFeatures f (rec {
    itoa."${deps.serde_json."1.0.40".itoa}".default = true;
    ryu."${deps.serde_json."1.0.40".ryu}".default = true;
    serde."${deps.serde_json."1.0.40".serde}".default = true;
    serde_json = fold recursiveUpdate {} [
      { "1.0.40"."indexmap" =
        (f.serde_json."1.0.40"."indexmap" or false) ||
        (f.serde_json."1.0.40".preserve_order or false) ||
        (serde_json."1.0.40"."preserve_order" or false); }
      { "1.0.40".default = (f.serde_json."1.0.40".default or true); }
    ];
  }) [
    (features_.itoa."${deps."serde_json"."1.0.40"."itoa"}" deps)
    (features_.ryu."${deps."serde_json"."1.0.40"."ryu"}" deps)
    (features_.serde."${deps."serde_json"."1.0.40"."serde"}" deps)
  ];


# end
# strsim-0.6.0

  crates.strsim."0.6.0" = deps: { features?(features_.strsim."0.6.0" deps {}) }: buildRustCrate {
    crateName = "strsim";
    version = "0.6.0";
    description = "Implementations of string similarity metrics.\nIncludes Hamming, Levenshtein, Damerau-Levenshtein, Jaro, and Jaro-Winkler.\n";
    authors = [ "Danny Guo <dannyguo91@gmail.com>" ];
    sha256 = "1lz85l6y68hr62lv4baww29yy7g8pg20dlr0lbaswxmmcb0wl7gd";
  };
  features_.strsim."0.6.0" = deps: f: updateFeatures f (rec {
    strsim."0.6.0".default = (f.strsim."0.6.0".default or true);
  }) [];


# end
# syn-0.15.39

  crates.syn."0.15.39" = deps: { features?(features_.syn."0.15.39" deps {}) }: buildRustCrate {
    crateName = "syn";
    version = "0.15.39";
    description = "Parser for Rust source code";
    authors = [ "David Tolnay <dtolnay@gmail.com>" ];
    sha256 = "0n5mv317yghjcgzm0ik9racfjx8srhwfgazm6y80wgmkfpwz8myy";
    dependencies = mapFeatures features ([
      (crates."proc_macro2"."${deps."syn"."0.15.39"."proc_macro2"}" deps)
      (crates."unicode_xid"."${deps."syn"."0.15.39"."unicode_xid"}" deps)
    ]
      ++ (if features.syn."0.15.39".quote or false then [ (crates.quote."${deps."syn"."0.15.39".quote}" deps) ] else []));
    features = mkFeatures (features."syn"."0.15.39" or {});
  };
  features_.syn."0.15.39" = deps: f: updateFeatures f (rec {
    proc_macro2 = fold recursiveUpdate {} [
      { "${deps.syn."0.15.39".proc_macro2}"."proc-macro" =
        (f.proc_macro2."${deps.syn."0.15.39".proc_macro2}"."proc-macro" or false) ||
        (syn."0.15.39"."proc-macro" or false) ||
        (f."syn"."0.15.39"."proc-macro" or false); }
      { "${deps.syn."0.15.39".proc_macro2}".default = (f.proc_macro2."${deps.syn."0.15.39".proc_macro2}".default or false); }
    ];
    quote = fold recursiveUpdate {} [
      { "${deps.syn."0.15.39".quote}"."proc-macro" =
        (f.quote."${deps.syn."0.15.39".quote}"."proc-macro" or false) ||
        (syn."0.15.39"."proc-macro" or false) ||
        (f."syn"."0.15.39"."proc-macro" or false); }
      { "${deps.syn."0.15.39".quote}".default = (f.quote."${deps.syn."0.15.39".quote}".default or false); }
    ];
    syn = fold recursiveUpdate {} [
      { "0.15.39"."clone-impls" =
        (f.syn."0.15.39"."clone-impls" or false) ||
        (f.syn."0.15.39".default or false) ||
        (syn."0.15.39"."default" or false); }
      { "0.15.39"."derive" =
        (f.syn."0.15.39"."derive" or false) ||
        (f.syn."0.15.39".default or false) ||
        (syn."0.15.39"."default" or false); }
      { "0.15.39"."parsing" =
        (f.syn."0.15.39"."parsing" or false) ||
        (f.syn."0.15.39".default or false) ||
        (syn."0.15.39"."default" or false); }
      { "0.15.39"."printing" =
        (f.syn."0.15.39"."printing" or false) ||
        (f.syn."0.15.39".default or false) ||
        (syn."0.15.39"."default" or false); }
      { "0.15.39"."proc-macro" =
        (f.syn."0.15.39"."proc-macro" or false) ||
        (f.syn."0.15.39".default or false) ||
        (syn."0.15.39"."default" or false); }
      { "0.15.39"."quote" =
        (f.syn."0.15.39"."quote" or false) ||
        (f.syn."0.15.39".printing or false) ||
        (syn."0.15.39"."printing" or false); }
      { "0.15.39".default = (f.syn."0.15.39".default or true); }
    ];
    unicode_xid."${deps.syn."0.15.39".unicode_xid}".default = true;
  }) [
    (features_.proc_macro2."${deps."syn"."0.15.39"."proc_macro2"}" deps)
    (features_.quote."${deps."syn"."0.15.39"."quote"}" deps)
    (features_.unicode_xid."${deps."syn"."0.15.39"."unicode_xid"}" deps)
  ];


# end
# thread_local-0.3.6

  crates.thread_local."0.3.6" = deps: { features?(features_.thread_local."0.3.6" deps {}) }: buildRustCrate {
    crateName = "thread_local";
    version = "0.3.6";
    description = "Per-object thread-local storage";
    authors = [ "Amanieu d'Antras <amanieu@gmail.com>" ];
    sha256 = "02rksdwjmz2pw9bmgbb4c0bgkbq5z6nvg510sq1s6y2j1gam0c7i";
    dependencies = mapFeatures features ([
      (crates."lazy_static"."${deps."thread_local"."0.3.6"."lazy_static"}" deps)
    ]);
  };
  features_.thread_local."0.3.6" = deps: f: updateFeatures f (rec {
    lazy_static."${deps.thread_local."0.3.6".lazy_static}".default = true;
    thread_local."0.3.6".default = (f.thread_local."0.3.6".default or true);
  }) [
    (features_.lazy_static."${deps."thread_local"."0.3.6"."lazy_static"}" deps)
  ];


# end
# toml-0.2.1

  crates.toml."0.2.1" = deps: { features?(features_.toml."0.2.1" deps {}) }: buildRustCrate {
    crateName = "toml";
    version = "0.2.1";
    description = "A native Rust encoder and decoder of TOML-formatted files and streams. Provides\nimplementations of the standard Encodable/Decodable traits for TOML data to\nfacilitate deserializing and serializing Rust structures.\n";
    authors = [ "Alex Crichton <alex@alexcrichton.com>" ];
    sha256 = "0p4rkaqhmk4fp6iqpxfgp3p98hxhbs2wmla3fq531n875h922yqs";
    dependencies = mapFeatures features ([
]);
  };
  features_.toml."0.2.1" = deps: f: updateFeatures f (rec {
    toml = fold recursiveUpdate {} [
      { "0.2.1"."rustc-serialize" =
        (f.toml."0.2.1"."rustc-serialize" or false) ||
        (f.toml."0.2.1".default or false) ||
        (toml."0.2.1"."default" or false); }
      { "0.2.1".default = (f.toml."0.2.1".default or true); }
    ];
  }) [];


# end
# ucd-util-0.1.3

  crates.ucd_util."0.1.3" = deps: { features?(features_.ucd_util."0.1.3" deps {}) }: buildRustCrate {
    crateName = "ucd-util";
    version = "0.1.3";
    description = "A small utility library for working with the Unicode character database.\n";
    authors = [ "Andrew Gallant <jamslam@gmail.com>" ];
    sha256 = "1n1qi3jywq5syq90z9qd8qzbn58pcjgv1sx4sdmipm4jf9zanz15";
  };
  features_.ucd_util."0.1.3" = deps: f: updateFeatures f (rec {
    ucd_util."0.1.3".default = (f.ucd_util."0.1.3".default or true);
  }) [];


# end
# unicode-xid-0.1.0

  crates.unicode_xid."0.1.0" = deps: { features?(features_.unicode_xid."0.1.0" deps {}) }: buildRustCrate {
    crateName = "unicode-xid";
    version = "0.1.0";
    description = "Determine whether characters have the XID_Start\nor XID_Continue properties according to\nUnicode Standard Annex #31.\n";
    authors = [ "erick.tryzelaar <erick.tryzelaar@gmail.com>" "kwantam <kwantam@gmail.com>" ];
    sha256 = "05wdmwlfzxhq3nhsxn6wx4q8dhxzzfb9szsz6wiw092m1rjj01zj";
    features = mkFeatures (features."unicode_xid"."0.1.0" or {});
  };
  features_.unicode_xid."0.1.0" = deps: f: updateFeatures f (rec {
    unicode_xid."0.1.0".default = (f.unicode_xid."0.1.0".default or true);
  }) [];


# end
# utf8-ranges-1.0.3

  crates.utf8_ranges."1.0.3" = deps: { features?(features_.utf8_ranges."1.0.3" deps {}) }: buildRustCrate {
    crateName = "utf8-ranges";
    version = "1.0.3";
    description = "Convert ranges of Unicode codepoints to UTF-8 byte ranges.";
    authors = [ "Andrew Gallant <jamslam@gmail.com>" ];
    sha256 = "0nkh73y241czrxagm77qz20qcfn3h54a6v9cpvc7wjzwkaaqkswp";
  };
  features_.utf8_ranges."1.0.3" = deps: f: updateFeatures f (rec {
    utf8_ranges."1.0.3".default = (f.utf8_ranges."1.0.3".default or true);
  }) [];


# end
# zmq-0.9.1

  crates.zmq."0.9.1" = deps: { features?(features_.zmq."0.9.1" deps {}) }: buildRustCrate {
    crateName = "zmq";
    version = "0.9.1";
    description = "High-level bindings to the zeromq library";
    authors = [ "a.rottmann@gmx.at" "erick.tryzelaar@gmail.com" ];
    edition = "2018";
    sha256 = "1g8i8kmsghavkf6hb99fmzjmy4cnr63rdlshcxdwhp7ihywf8i75";
    build = "build.rs";
    dependencies = mapFeatures features ([
      (crates."bitflags"."${deps."zmq"."0.9.1"."bitflags"}" deps)
      (crates."libc"."${deps."zmq"."0.9.1"."libc"}" deps)
      (crates."log"."${deps."zmq"."0.9.1"."log"}" deps)
      (crates."zmq_sys"."${deps."zmq"."0.9.1"."zmq_sys"}" deps)
    ]);
    features = mkFeatures (features."zmq"."0.9.1" or {});
  };
  features_.zmq."0.9.1" = deps: f: updateFeatures f (rec {
    bitflags."${deps.zmq."0.9.1".bitflags}".default = true;
    libc."${deps.zmq."0.9.1".libc}".default = true;
    log."${deps.zmq."0.9.1".log}".default = true;
    zmq = fold recursiveUpdate {} [
      { "0.9.1"."zmq_has" =
        (f.zmq."0.9.1"."zmq_has" or false) ||
        (f.zmq."0.9.1".default or false) ||
        (zmq."0.9.1"."default" or false); }
      { "0.9.1".default = (f.zmq."0.9.1".default or true); }
    ];
    zmq_sys = fold recursiveUpdate {} [
      { "${deps.zmq."0.9.1".zmq_sys}"."vendored" =
        (f.zmq_sys."${deps.zmq."0.9.1".zmq_sys}"."vendored" or false) ||
        (zmq."0.9.1"."vendored" or false) ||
        (f."zmq"."0.9.1"."vendored" or false); }
      { "${deps.zmq."0.9.1".zmq_sys}".default = true; }
    ];
  }) [
    (features_.bitflags."${deps."zmq"."0.9.1"."bitflags"}" deps)
    (features_.libc."${deps."zmq"."0.9.1"."libc"}" deps)
    (features_.log."${deps."zmq"."0.9.1"."log"}" deps)
    (features_.zmq_sys."${deps."zmq"."0.9.1"."zmq_sys"}" deps)
  ];


# end
# zmq-sys-0.9.1

  crates.zmq_sys."0.9.1" = deps: { features?(features_.zmq_sys."0.9.1" deps {}) }: buildRustCrate {
    crateName = "zmq-sys";
    version = "0.9.1";
    description = "Low-level bindings to the zeromq library";
    authors = [ "a.rottmann@gmx.at" "erick.tryzelaar@gmail.com" ];
    sha256 = "0b3l02ag621ks5wbk9dma9y51jz420k2iy19d3rhjpb0qxq2yl6g";
    build = "build/main.rs";
    dependencies = mapFeatures features ([
      (crates."libc"."${deps."zmq_sys"."0.9.1"."libc"}" deps)
    ]);

    buildDependencies = mapFeatures features ([
      (crates."metadeps"."${deps."zmq_sys"."0.9.1"."metadeps"}" deps)
    ]);
    features = mkFeatures (features."zmq_sys"."0.9.1" or {});
  };
  features_.zmq_sys."0.9.1" = deps: f: updateFeatures f (rec {
    libc."${deps.zmq_sys."0.9.1".libc}".default = true;
    metadeps."${deps.zmq_sys."0.9.1".metadeps}".default = true;
    zmq_sys = fold recursiveUpdate {} [
      { "0.9.1"."zeromq-src" =
        (f.zmq_sys."0.9.1"."zeromq-src" or false) ||
        (f.zmq_sys."0.9.1".vendored or false) ||
        (zmq_sys."0.9.1"."vendored" or false); }
      { "0.9.1".default = (f.zmq_sys."0.9.1".default or true); }
    ];
  }) [
    (features_.libc."${deps."zmq_sys"."0.9.1"."libc"}" deps)
    (features_.metadeps."${deps."zmq_sys"."0.9.1"."metadeps"}" deps)
  ];


# end
}
