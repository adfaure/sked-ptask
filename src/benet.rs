extern crate serde_json;

extern crate interval_set;

use self::interval_set::{Interval, IntervalSet, ToIntervalSet};
use batsim::*;
use serde_json::from_value;
use std::cmp::Ordering;
use std::convert::TryInto;

use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::LinkedList;

struct RunningJob<'a>(&'a IntervalSet, &'a f64);

type Mapping = HashMap<String, String>;

pub struct Benet {
    pub nb_resources: u32,
    pub resources: IntervalSet,
    pub time: f64,
    pub running_jobs: HashMap<String, (Job, IntervalSet, f64)>,
    pub job_queue: LinkedList<Job>,
    pub config: serde_json::Value,
    pub mappings:HashMap< String, Mapping>,
}

impl Benet {
    pub fn new() -> Benet {
        Benet {
            nb_resources: 0,
            time: 0.0,
            resources: IntervalSet::empty(),
            running_jobs: HashMap::new(),
            job_queue: LinkedList::new(),
            config: json!(null),
            mappings: HashMap::new(),
        }
    }

    fn schedule_job(&mut self, _: f64) -> Option<Vec<BatsimEvent>> {
        let mut res: Vec<BatsimEvent> = Vec::new();
        let mut optional = self.job_queue.pop_front();

        while let Some(job) = optional {
            match self.find_job_allocation(&self.resources, &job) {
                None => {
                    trace!("Cannot launch job={} now", job.id);
                    self.job_queue.push_front(job);
                    optional = None;
                }
                Some(allocation) => {
                    let alloc_str = format!("{}", allocation);
                    let mapping = self.mappings.remove(&job.id);
                    res.push(allocate_job_event(self.time, &job, alloc_str, mapping));
                    self.launch_job_internal(job.clone(), allocation.clone());
                    optional = self.job_queue.pop_front();
                }
            }
        }
        Some(res)
    }

    fn job_finished_internal(&mut self, job_id: String) {
        let (_, allocation, _) = self.running_jobs.remove(&job_id).unwrap();
        self.resources = self.resources.clone().union(allocation);
    }

    fn launch_job_internal(&mut self, job: Job, allocation: IntervalSet) {
        trace!("Launching Job::Job={} allocation={}", job.id, allocation);

        let term_time = self.time + job.walltime;
        self.resources = self.resources.clone().difference(allocation.clone());
        self.running_jobs
            .insert(job.id.clone(), (job, allocation, term_time));
    }

    fn find_job_allocation(&self, resources: &IntervalSet, job: &Job) -> Option<IntervalSet> {
        let current_available_size = resources.size();
        if current_available_size < (job.res as u32) {
            trace!(
                "No allocation possible yet for the job {} (nb res={}) (size available={})",
                job.id,
                job.res,
                current_available_size
            );
            return None;
        }

        trace!("Try to allocate Job={} res={}", job.id, job.res);

        let mut iter = resources.iter();
        let mut allocation = IntervalSet::empty();
        let mut left = job.res as u32;

        let mut interval = iter.next().unwrap();
        while allocation.size() != (job.res as u32) {
            // Note that we test earlier in the function if the interval
            // has enough resources, so this loop should not fail.
            let interval_size = interval.range_size();

            if interval_size > left {
                allocation.insert(Interval::new(
                    interval.get_inf(),
                    interval.get_inf() + left - 1,
                ));
            } else if interval_size == left {
                allocation.insert(interval.clone());
            } else if interval_size < left {
                allocation.insert(interval.clone());
                left -= interval_size;
                interval = iter.next().unwrap();
            }
        }
        Some(allocation)
    }
}

// The order is intentionally reversed because it is meant to be used into
// a priority queue, where the smallest time are have an higher prioriy.
impl<'a> Ord for RunningJob<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.1 < other.1 {
            return Ordering::Greater;
        } else if self.1 == other.1 {
            if self.0.size() > other.0.size() {
                return Ordering::Greater;
            } else if self.0.size() == other.0.size() {
                return Ordering::Equal;
            }
        }
        return Ordering::Less;
    }
}

impl<'a> PartialOrd for RunningJob<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> PartialEq for RunningJob<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.1 == other.1
    }
}

impl<'a> Eq for RunningJob<'a> {}

impl Scheduler for Benet {
    fn simulation_begins(
        &mut self,
        timestamp: &f64,
        nb_resources: i32,
        config: serde_json::Value,
    ) -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        self.config = config;
        self.nb_resources = nb_resources as u32;

        if self.config["resources"].is_string() {
            self.resources =
                String::from(self.config["resources"].as_str().unwrap()).to_interval_set();
            info!("Resources from config(={})", self.resources.to_string());
        } else {
            self.resources = Interval::new(0, self.nb_resources - 1).to_interval_set();
            info!(
                "Resources from nb of resources(={})",
                self.resources.to_string()
            );
        }

        info!(
            "Benet Initialized with {} resources ({})",
            self.resources.size(),
            self.resources
        );

        // We tell batsim that it does not need to wait for us
        Some(vec![notify_event(
            *timestamp,
            String::from("registration_finished"),
        )])
    }

    fn on_job_submission(
        &mut self,
        timestamp: &f64,
        job: Job,
        _: Option<Profile>,
    ) -> Option<Vec<BatsimEvent>> {
        trace!("Received job(={})", job);
        self.time = *timestamp;
        let mut job_copy = job.clone();
        // The job may have a mapping. In this case, the real number of resources need to be
        // computed from the mapping.
        if job.other.contains_key("mapping") {
            trace!("Job has a mapping: {}", job.other["mapping"]);;
            // iterate over everything.
            let mapping_object: serde_json::Value = job.other["mapping"].clone();
            let mapping: Mapping = from_value(mapping_object).unwrap();

            let mut executors: HashSet<String> = HashSet::new();
            trace!("mapping: {:?}", mapping);
            for (_rank, executor) in &mapping {
                executors.insert(executor.clone());
            }
            trace!("Number of resources from mappnig: {}", executors.len());
            job_copy.res = executors.len().try_into().unwrap();
            self.mappings.insert(job.id.clone(), mapping);
        }

        if job_copy.res as usize > self.nb_resources as usize {
            trace!(
                "Job(={:?}) requests to many resources for this cluster, max is : {}",
                job_copy, self.nb_resources
            );
            return Some(vec![reject_job_event(self.time, &job_copy)]);
        }

        self.job_queue.push_back(job_copy);
        None
    }

    fn on_job_completed(
        &mut self,
        timestamp: &f64,
        job_id: String,
        job_state: String,
        return_code: i32,
        alloc: String,
    ) -> Option<Vec<BatsimEvent>> {
        self.time = *timestamp;
        trace!(
            "Job={} terminated with Status: {}, return_code: {}",
            job_id,
            job_state,
            return_code
        );
        self.job_finished_internal(job_id);
        None
    }

    fn on_simulation_ends(&mut self, timestamp: &f64) {
        self.time = *timestamp;
        println!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(
        &mut self,
        _timestamp: &f64,
        _job_ids: Vec<String>,
    ) -> Option<Vec<BatsimEvent>> {
        panic!("Not implemented")
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        trace!("Respond to batsim at: {}", timestamp);
        self.schedule_job(*timestamp)
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}
