#[macro_use]
extern crate log;
extern crate env_logger;

#[macro_use]
extern crate serde_derive;
extern crate docopt;

use docopt::Docopt;

extern crate batsim;

#[macro_use]
extern crate serde_json;

mod benet;
mod fcfs;

static VERSION: &'static str = "1.2.3";
const USAGE: &'static str = "
Usage:
    sked --version
    sked [ --port=<port> ] [ --help]
    sked --help

Examples:
    sked -e

Options:
    -h --help                                       Show this screen.
    -v --version                                    Print version.
    -s --port=<port>                                The port number to bind the socket on.
";

#[derive(Debug, Deserialize)]
struct Args {
    flag_version: bool,
    flag_port: Option<i64>,
}

fn main() {
    env_logger::init().unwrap();
    info!("starting up");

    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    if args.flag_version {
        println!("{}", VERSION);
        return;
    }

    match args.flag_port {
        Some(1..=1023) => warn!("Port specified needs root privileges"),
        Some(port) => {
            if port > 65535 {
                panic!("Tcp port not available (max is 65535, given: {})", port)
            }
        }
        None => {}
    }

    info!("Initialize scheduler specified port: {:?}", args.flag_port);

    // let mut scheduler = fcfs::FCFS::new(false);
    let mut scheduler = benet::Benet::new();
    let mut batsim = batsim::Batsim::new(&mut scheduler, args.flag_port);
    batsim.run_simulation().unwrap();

    info!("Simulation finished");
}
