{
  pkgs ? import (
      fetchTarball "https://github.com/NixOS/nixpkgs-channels/archive/b58ada326aa612ea1e2fb9a53d550999e94f1985.tar.gz"
  ) { overlays = [ (import (fetchTarball "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz")) ]; }
}:
with pkgs;
with buildRustCrateHelpers;
let
  jobs = rec {
#    moz_nixpkgs = import "${nixpkgs}" { overlays = [ (import "${moz_overlay}") ]; };
    rustc = (pkgs.rustChannelOf {  channel = "stable"; }).rust;

    crates = (import ./Cargo.nix {
      inherit (pkgs) lib buildPlatform buildRustCrateHelpers fetchgit;
      buildRustCrate = pkgs.buildRustCrate.override {
        inherit rustc; # I guess that injection happens here?
      };
      cratesIO = import ./crates-io.nix {
        inherit (pkgs) lib buildRustCrate buildRustCrateHelpers;
      };
    });

    sked = (crates.ptasked {}).override {
      crateOverrides = defaultCrateOverrides // {
        zmq-sys = attrs: { propagatedBuildInputs = [ zeromq pkgconfig ];};
      };
    };

  };
in
  jobs.sked
