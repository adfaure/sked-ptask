{
  pkgs ? import (
      fetchTarball "https://github.com/NixOS/nixpkgs-channels/archive/b58ada326aa612ea1e2fb9a53d550999e94f1985.tar.gz"
  ) { overlays = [ (import (fetchTarball "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz")) ]; },
  kapack ? import (
    fetchTarball
    "https://github.com/oar-team/kapack/archive/master.tar.gz") { pkgs = pkgs; }
}:

with kapack;
with pkgs;

pkgs.mkShell {
  name = "rust_overlay_shell";
  buildInputs = [
    # pkgs.latest.rustChannels.nightly.rust
    (pkgs.rustChannelOf { channel = "stable"; }).rust
    batsim
    batexpe
    batsched
    pkgs.zeromq
    # So cargo can find zmq
    pkgs.pkgconfig
    carnix
  ];

  shellHook = ''
    export RUST_SRC_PATH="${pkgs.rustc.src}/src"
  '';
}
